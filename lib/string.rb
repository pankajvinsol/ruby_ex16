class String
  def highlight_search(pattern)
    regex_pattern = Regexp.new(pattern, 'i')
    count = 0

    string = gsub(regex_pattern) do |match|
      count += 1
      "(#{ match })"
    end

    [string, count]
  end
end

