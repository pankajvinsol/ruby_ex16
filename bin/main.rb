require_relative '../lib/string.rb'

print "Enter a line : "
line = gets.chomp
print "Enter search string : "
pattern = gets.chomp

line, count = line.highlight_search(pattern)

puts line
puts "Total Occurences found: #{ count }"

